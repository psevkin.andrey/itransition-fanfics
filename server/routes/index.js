const express = require('express');
const router = express.Router();
const passport = require('passport');
const indexCtrl = require('../controllers/indexController');
const elastic = require('../config/elasticsearch');

router.get('/auth/facebook', passport.authenticate('facebook', {
    scope : ['public_profile', 'email']
}));


router.get('/auth/facebook/callback/',
    passport.authenticate('facebook'), indexCtrl.sendToken);


router.get('/auth/twitter', passport.authenticate('twitter'));


router.get('/auth/twitter/callback/',
    passport.authenticate('twitter'), indexCtrl.sendTwitterToken);

router.get('/auth/vkontakte', passport.authenticate('vkontakte'));


router.get('/auth/vkontakte/callback/',
    passport.authenticate('vkontakte'), indexCtrl.sendToken);


/* GET suggestions */
router.get('/search/:input', function (req, res, next) {
    elastic.getSuggestions(req.params.input).then(function (result) {
        res.json(result.hits.hits.map(el => el._source));
    })
        .catch(e => {});
});

router.post('/', function (req, res, next) {
    elastic.addFanfic(req.body).then(function (result) { res.json(result) });
});

module.exports = router;

