const express = require('express');
const router = express.Router();
const fanficCtrl = require('../controllers/fanficController');
const fileParser = require('connect-multiparty')();
const jwt = require('express-jwt');
const config = require('../config/config.json');

const jwtCheck = jwt({
    secret: config.jwt.secret,
    audience: config.jwt.audience,
    issuer: config.jwt.issuer
});


router.get('/', fanficCtrl.getFanficList);
router.get('/rating', jwtCheck, fanficCtrl.getAllRates);
router.get('/tags', fanficCtrl.getTags);
router.get('/get/:id', fanficCtrl.getFanfic);
router.get('/newest', fanficCtrl.getNewerFanfics);
router.get('/popular', fanficCtrl.getPopularFanfics);
router.get('/tag/:tag', fanficCtrl.getFanficsByTag);
router.get('/genre/:genre', fanficCtrl.getFanficsByGenre);
router.get('/search', fanficCtrl.searchFanfic);
router.post('/create', fileParser, jwtCheck, fanficCtrl.createFanfic);
router.post('/rate', jwtCheck, fanficCtrl.setRating);
router.put('/update/:id', fileParser, jwtCheck, fanficCtrl.updateFanfic);
router.delete('/delete/:id', jwtCheck, fanficCtrl.deleteFanfic);


module.exports = router;
