const express = require('express');
const router = express.Router();
const usersCtrl = require('../controllers/usersController');
const jwt = require('express-jwt');
const config = require('../config/config.json');
const fileParser = require('connect-multiparty')();

const jwtCheck = jwt({
    secret: config.jwt.secret,
    audience: config.jwt.audience,
    issuer: config.jwt.issuer
});


router.get('/', usersCtrl.getUserList);
router.get('/verify', usersCtrl.verifyEmail);
router.get('/get/:id', jwtCheck, usersCtrl.getUser);
router.post('/login', usersCtrl.sign_in);
router.post('/create',fileParser, usersCtrl.createUser);
router.delete('/delete/:id', jwtCheck, usersCtrl.deleteUser);
router.put('/update/:id', jwtCheck, fileParser, usersCtrl.updateUser);
router.put('/block/:id', jwtCheck, usersCtrl.block);
router.put('/admin/:id', jwtCheck, usersCtrl.admin);


module.exports = router;
