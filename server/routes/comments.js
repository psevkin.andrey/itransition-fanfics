const express = require('express');
const router = express.Router();
const commentCtrl = require('../controllers/commentController');
const jwt = require('express-jwt');
const config = require('../config/config.json');

const jwtCheck = jwt({
    secret: config.jwt.secret,
    audience: config.jwt.audience,
    issuer: config.jwt.issuer
});

router.get('/', commentCtrl.getComments);
router.post('/create', jwtCheck, commentCtrl.createComment);
router.delete('/delete/:id',jwtCheck, commentCtrl.deleteComment);
router.put('/get/:id/like', jwtCheck, commentCtrl.likeComment);
router.put('/get/:id/unlike', jwtCheck, commentCtrl.unlikeComment);
router.get('/get/likes', jwtCheck, commentCtrl.getLikes);

module.exports = router;