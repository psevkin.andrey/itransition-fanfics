'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Users', [
            {
                username: 'admin',
                email: 'admin@admin.com',
                password: '$2a$08$Dk5WHffl4a9Oh1wz7iGgB.KBa7edp0QPcg/GmsrYeiAFw/qiG3YEe',
                admin: true,
                verified_email: true,
                blocked: false,
                createdAt: '12.02.2018',
                updatedAt: '12.02.2018',
            },
            {
                username: 'common.user',
                email: 'common.user@example.com',
                password: '$2a$08$Dk5WHffl4a9Oh1wz7iGgB.KBa7edp0QPcg/GmsrYeiAFw/qiG3YEe',
                verified_email: true,
                admin: false,
                blocked: false,
                createdAt: '12.02.2018',
                updatedAt: '12.02.2018',
            }], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Users', null, {});
    }
};
