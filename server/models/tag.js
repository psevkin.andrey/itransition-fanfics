'use strict';
module.exports = (sequelize, DataTypes) => {
    const Tag = sequelize.define('Tag', {
        value: DataTypes.STRING
    });

    Tag.associate = models => {
        Tag.belongsToMany(models.Fanfic, { through: 'FanficTag' })
    };

    return Tag;
};