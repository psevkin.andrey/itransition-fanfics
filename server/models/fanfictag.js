'use strict';
module.exports = (sequelize, DataTypes) => {
    const FanficTag = sequelize.define('FanficTag', {
        FanficId: DataTypes.INTEGER,
        TagId: DataTypes.INTEGER
    });

    return FanficTag;
};