'use strict';
module.exports = (sequelize, DataTypes) => {
    const Chapter = sequelize.define('Chapter', {
        number: DataTypes.INTEGER,
        title: DataTypes.STRING,
        content: DataTypes.TEXT,
        image: DataTypes.STRING,
        rating: DataTypes.FLOAT,
        qty: DataTypes.INTEGER
    });

    Chapter.associate = models => {
        Chapter.belongsTo(models.Fanfic);
    };


    return Chapter;
};