'use strict';
module.exports = (sequelize, DataTypes) => {
    const Comment = sequelize.define('Comment', {
        title: DataTypes.STRING,
        content: DataTypes.TEXT,
        likes: DataTypes.INTEGER,
        UserId: DataTypes.INTEGER
    });

    Comment.associate = models => {
        Comment.belongsTo(models.User);
        Comment.belongsTo(models.Fanfic);
        Comment.belongsToMany(models.User, { through: 'Like' })
    };
    return Comment;
};