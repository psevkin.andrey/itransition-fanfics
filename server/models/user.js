'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        username: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        hash_key: DataTypes.STRING,
        image: DataTypes.STRING,
        admin: DataTypes.BOOLEAN,
        blocked: DataTypes.BOOLEAN,
        verified_email: DataTypes.BOOLEAN
    });

    User.associate = models => {
        User.hasMany(models.Fanfic);
        User.hasMany(models.Comment);
        User.belongsToMany(models.Comment, { through: 'Like' })
    };

    User.beforeCreate((user, options) => {
        user.password = bcrypt.hashSync(user.password, 8);
    });


    return User;
};