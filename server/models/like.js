'use strict';
module.exports = (sequelize, DataTypes) => {
    const Like = sequelize.define('Like', {
        UserId: DataTypes.INTEGER,
        CommentId: DataTypes.INTEGER
    });
    return Like;
};