'use strict';
module.exports = (sequelize, DataTypes) => {
    const Rating = sequelize.define('Rating', {
        UserId: DataTypes.INTEGER,
        ChapterId: DataTypes.INTEGER,
        rating: DataTypes.INTEGER
    });
    return Rating;
};