'use strict';
module.exports = (sequelize, DataTypes) => {
    const Fanfic = sequelize.define('Fanfic', {
        title: DataTypes.STRING,
        description: DataTypes.TEXT,
        image: DataTypes.STRING,
        genre: DataTypes.STRING,
        rating: DataTypes.FLOAT
    });

    Fanfic.associate = models => {
        Fanfic.belongsTo(models.User);
        Fanfic.hasMany(models.Comment);
        Fanfic.hasMany(models.Chapter);
        Fanfic.belongsToMany(models.Tag, { through: 'FanficTag' })
    };

    return Fanfic;
};