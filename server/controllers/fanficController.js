const sendJSONResponse = require('./utils');
const models = require('../models');
const cloudinary = require('cloudinary');
const config = require('../config/config.json');
const elastic = require('../config/elasticsearch');

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

module.exports.getFanficList = (req, res) => {
    models.Fanfic.findAll({
        include: [models.Tag, models.Chapter, models.User]
    }).then(fanfics => {
        sendJSONResponse(res, 200, fanfics);
    });
};

const createTags = (fanfic, tagObj) => {
    models.Tag
        .findOrCreate({
            where: {value: tagObj.value},
            defaults: {value: tagObj.value}
        })
        .then(tag => {
            fanfic.addTag(tag[0]);
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });
};
const createChapters = (fanfic, chapterObj, image) => {
    cloudinary.uploader.upload(image, (result) => {
        models.Chapter.create({
            number: chapterObj.number,
            title: chapterObj.title,
            content: chapterObj.content,
            image: result.url || chapterObj.image,
            FanficId: fanfic.id
        }).then(chapter => {
            fanfic.addChapter(chapter);
        }).catch(err => {
            console.log(err);
        });
    });

};

module.exports.createFanfic = (req, res) => {

    const content = JSON.parse(req.body.content);
    const tags = content.Tags;
    const chapters = content.Chapters;
    const images = req.files;
    const fanficImage = images && images.fanfic && images.fanfic.path;


    cloudinary.uploader.upload(fanficImage, function (result) {
        models.Fanfic.create({
            title: content.title,
            description: content.description,
            image: result.url,
            genre: content.genre,
            UserId: content.UserId,
        }).then(fanfic => {
            for (let tag of tags) {
                createTags(fanfic, tag);
            }
            for (let chapter of chapters) {
                let image = images && images[`chapter${chapter.number}`] && images[`chapter${chapter.number}`].path;
                createChapters(fanfic, chapter, image);
            }
            sendJSONResponse(res, 201, fanfic);
        }).catch(err => {
            sendJSONResponse(res, 400, err);
        });
    });
};

const deleteTags = (fanfic) => {
    models.FanficTag.destroy({
        where: {FanficId: fanfic.id}
    }).catch(err => console.log(err.errors));
};

module.exports.deleteFanfic = (req, res) => {
    models.Fanfic
        .findById(req.params.id)
        .then(fanfic => {
            if (fanfic) {
                fanfic
                    .destroy()
                    .then(fanfic => {
                        sendJSONResponse(res, 200, fanfic);
                    })
                    .catch(err => {
                        sendJSONResponse(res, 400, err.errors);
                    });
            } else {
                sendJSONResponse(res, 404, 'fanfic not found');
            }
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });
};

const destroyTags = (id) => {
    models.FanficTag.findAll({where: {FanficId: id}}).then(tags => {
        if (tags) {
            for (let tag of tags) {
                tag.destroy();
            }
        }
    });
};

const updateChapters = (updatedFanfic, chapter, image) => {
    cloudinary.uploader.upload(image, (result) => {
        models.Chapter.findById(chapter.id).then(dbChapter => {
            if (dbChapter) {
                dbChapter.update({
                    number: chapter.number,
                    title: chapter.title,
                    content: chapter.content,
                    image: result.url || chapter.image
                });
            } else {
                createChapters(updatedFanfic, chapter, image);
            }
        }).catch(err => cosole.log(err));
    });
};

const filterChapters = (dbChapters, chapters) => {
    return dbChapters.filter(chapter =>
        chapters.findIndex((item, index, array) => {
            if (item.id === chapter.id) {
                chapters.splice(index, 1);
                return true;
            }
            return false;
        }) === -1);
};

const destroyChapters = (res, id, chapters) => {
    models.Chapter.findAll({where: {FanficId: id}}).then(dbChapters => {
        dbChapters = filterChapters(dbChapters, chapters);
        for (let chapter of dbChapters) {
            chapter.destroy();
        }
    }).catch(err => console.log(err));
};

const fanficUpdateImage = (id, image) => {
    cloudinary.uploader.upload(image, (result) => {
        models.Fanfic.findById(id).then(fanfic => {
            fanfic.update({
                image: result.url || fanfic.image
            }).catch(err => console.log(err));
        }).catch();
    });
};

const updateFanficInfo = (content, images) => {
    const tags = content.Tags;
    const chapters = content.Chapters;

    destroyChapters(res, content.id, this.chapters);
    destroyTags(content.id);

    models.Fanfic.findById(content.id)
        .then(fanfic => {
            if (fanfic) {
                fanfic.update({
                    title: content.title,
                    description: content.description,
                    genre: content.genre
                }).then(updatedFanfic => {
                    for (let tag of tags) {
                        createTags(updatedFanfic, tag);
                    }
                    for (let chapter of chapters) {
                        let image = images && images[`chapter${chapter.number}`] && images[`chapter${chapter.number}`].path;
                        updateChapters(updatedFanfic, chapter, image);
                    }
                    sendJSONResponse(res, 200, 'Fanfic successfully updated');
                }).catch(err => {
                    sendJSONResponse(res, 400, err.errors);
                });
            } else {
                sendJSONResponse(res, 404, 'Fanfic not found');
            }
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });

};

module.exports.updateFanfic = (req, res) => {

    const content = JSON.parse(req.body.content);
    const images = req.files;
    const fanficImage = images && images.fanfic && images.fanfic.path;

    fanficUpdateImage(content.id, fanficImage);
    updateFanficInfo(content, images);
};

module.exports.getFanfic = (req, res) => {
    models.Fanfic
        .findById(req.params.id, {
            include: [models.Chapter, models.Tag, models.User, models.Comment]
        })
        .then(fanfic => {
            if (fanfic) {
                sendJSONResponse(res, 200, fanfic);
            } else {
                sendJSONResponse(res, 404, 'fanfic not found');
            }
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });
};

module.exports.getTags = (req, res) => {
    models.Tag.findAll({}).then(tags => {
        sendJSONResponse(res, 200, tags);
    }).catch(err => {
        sendJSONResponse(res, 400, err.errors);
    });
};

module.exports.setRating = (req, res) => {
    models.Rating.findOrCreate({
        where: {
            UserId: req.body.UserId,
            ChapterId: req.body.ChapterId
        },
        defaults: {
            UserId: req.body.UserId,
            ChapterId: req.body.ChapterId,
            rating: req.body.rating
        }
    }).then(rating => {
        if (rating[1]) {
            models.Chapter.findById(req.body.ChapterId, {}).then(chapter => {
                if (chapter) {
                    chapter.update({
                        rating: (+chapter.rating) + (+req.body.rating),
                        qty: ++chapter.qty
                    }).then(updatedChapter => {
                        models.Fanfic.findById(updatedChapter.FanficId, {
                            include: [models.Chapter]
                        }).then(fanfic => {
                            let rate = 0;
                            let qty = fanfic.Chapters.length;
                            for (let chapter of fanfic.Chapters) {
                                if (chapter.qty !== 0) {
                                    rate += chapter.rating / chapter.qty;
                                } else {
                                    qty--;
                                }

                            }
                            rate = qty === 0 ? 0 : rate / qty;
                            console.log(qty);
                            console.log(rate);
                            fanfic.update({
                                rating: rate
                            }).then(result => {
                                sendJSONResponse(res, 200, 'You successfully rated this chapter!');
                            }).catch();
                        })
                    }).catch();
                } else {
                    sendJSONResponse(res, 404, 'Chapter not found');
                }
            }).catch();
        } else {
            sendJSONResponse(res, 200, 'You already rated this chapter');
        }
    }).catch();
};

module.exports.getAllRates = (req, res) => {
    models.Rating.findAll({where: {UserId: req.query.UserId}})
        .then(rating => {
            sendJSONResponse(res, 200, rating);
        })
        .catch(err => sendJSONResponse(res, 400, err.errors));
};

module.exports.getNewerFanfics = (req, res) => {
    models.Fanfic.findAll({
        limit: +req.query.qty,
        order: [['createdAt', 'DESC']]
    })
        .then(fanfics => sendJSONResponse(res, 200, fanfics))
        .catch(err => sendJSONResponse(res, 400, err.errors));
};

module.exports.getPopularFanfics = (req, res) => {
    models.Fanfic.findAll({
        limit: +req.query.qty,
        order: [['rating', 'DESC']]
    })
        .then(fanfics => sendJSONResponse(res, 200, fanfics))
        .catch(err => sendJSONResponse(res, 400, err.errors));
};

module.exports.getFanficsByTag = (req, res) => {
    models.Fanfic.findAll({
        include: [{
            model: models.Tag,
            where: {value: req.params.tag}
        }]
    })
        .then(fanfics => sendJSONResponse(res, 200, fanfics))
        .catch(err => sendJSONResponse(res, 400, err.errors));
};

module.exports.getFanficsByGenre = (req, res) => {
    models.Fanfic.findAll({
        where: {genre: req.params.genre}
    })
        .then(fanfics => sendJSONResponse(res, 200, fanfics))
        .catch(err => sendJSONResponse(res, 400, err.errors));
};

module.exports.searchFanfic = (req, res) => {
    elastic.getSuggestions(req.query.search)
        .then(result => sendJSONResponse(res, 200, result))
        .catch(err => sendJSONResponse(res, 400, err));
};