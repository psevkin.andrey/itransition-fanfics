module.exports = (res, status, content) => {
    res.status(status);
    res.json(content);
};