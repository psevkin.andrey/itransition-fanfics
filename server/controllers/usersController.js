const sendJSONResponse = require('./utils');
const models = require('../models');
const bcrypt = require('bcrypt');
const nodemailer = require("nodemailer");
const config = require('../config/config.json');
const jwt = require('jsonwebtoken');
const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});


const smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: "itransition.fanfics@gmail.com",
        pass: "itransition_project"
    }
});

const sendConfirmationEmail = (req, res, user) => {

    let link = `http://${req.get('host')}/api/users/verify?id=${user.id}&token=${user.hash_key}`;
    const mailOptions = {
        to: user.email,
        subject: "Please confirm your Email account",
        html: "Hello,<br> Please Click on the link to verify your email.<br><a href=" + link + ">Click here to verify</a>"
    };

    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            sendJSONResponse(res, 400, {
                message: error
            });
        } else {
            sendJSONResponse(res, 201, [{
                message: "Email successfully sent!"
            }])
        }
    });
};

const emailConfirmedMessage = (req, res, user) => {

    let link = `http://${req.get('host')}/api/users/verify?id=${user.id}&token=${user.hash_key}`;
    const mailOptions = {
        to: user.email,
        subject: "Email successfully confirmed!",
        html: "Thx for email confirmation!"
    };

    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            sendJSONResponse(res, 400, {
                message: error
            });
        } else {
            sendJSONResponse(res, 200, user);
        }
    });
};

const verifyPassword = (password, hash_password) => {
    return bcrypt.compareSync(password, hash_password);
};

const verificateUser = (req, res, user) => {

    if (verifyPassword(req.body.password, user.password)) {
        user.password = undefined;
        sendJSONResponse(res, 200,
            {
                token: jwt.sign({user: user}, config.jwt.secret, {
                    expiresIn: '12h',
                    audience: config.jwt.audience,
                    issuer: config.jwt.issuer
                })
            }
        );
    } else {
        sendJSONResponse(res, 401, [{
            message: 'Authentication failed. Wrong password.'
        }]);
    }
};

module.exports.getUserList = (req, res) => {
    models.User.findAll({
        attributes: {
            exclude: ['password', 'hash_key']
        }
    }).then(users => {
        sendJSONResponse(res, 200, users);
    });
};

module.exports.createUser = (req, res) => {
    let content = JSON.parse(req.body.content);
    let image = req.files && req.files.avatar && req.files.avatar.path;

    cloudinary.uploader.upload(image, function (result) {
        models.User.create({
            username: content.username,
            email: content.email,
            password: content.password,
            image: result.url || 'http://res.cloudinary.com/urfather/image/upload/v1518342315/default-user-image.png',
            admin: content.admin,
            hash_key: bcrypt.hashSync(Math.floor((Math.random() * 100) + 54) + "", 8),
            blocked: content.blocked,
            verified_email: content.verified_email
        })
            .then(user => {
                if (!user.verified_email) {
                    sendConfirmationEmail(req, res, user);
                } else {
                    sendJSONResponse(res, 200, 'User created successfully!');
                }
            }).catch((err) => {
            sendJSONResponse(res, 400, err.errors);
        });
    });
};

module.exports.verifyEmail = (req, res) => {
    let id = +req.query.id;
    let token = req.query.token;

    models.User
        .findById(id)
        .then(user => {
            if (user.hash_key === token) {
                user
                    .update({
                        hash_key: null,
                        verified_email: true
                    })
                    .then(user => {
                        user.password = undefined;
                        user.hash_key = undefined;
                        emailConfirmedMessage(req, res, user);
                    })
                    .catch(err => {
                        sendJSONResponse(res, 400, err.errors);
                    });
            } else {
                sendJSONResponse(res, 401, {
                    message: 'bad token'
                });
            }
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });
};

module.exports.updateUser = (req, res) => {

    let content = JSON.parse(req.body.content);
    let image = req.files && req.files.avatar && req.files.avatar.path;

    let password = content.password ? bcrypt.hashSync(content.password, 8) : undefined;
    cloudinary.uploader.upload(image, function (result) {
        models.User.findById(req.params.id)
            .then(user => {
                if (user) {
                    user.update({
                        username: content.username,
                        email: content.email,
                        password: password,
                        image: result.url || 'http://res.cloudinary.com/urfather/image/upload/v1518342315/default-user-image.png',
                        admin: content.admin,
                        blocked: content.blocked,
                        verified_email: content.verified_email
                    }).then(user => {
                        sendJSONResponse(res, 200, user);
                    }).catch(err => {
                        sendJSONResponse(res, 400, err.errors);
                    });
                } else {
                    sendJSONResponse(res, 404, [{message: "user not found"}]);
                }
            })
            .catch(err => {
                sendJSONResponse(res, 400, err.errors);
            });
    });
};

module.exports.deleteUser = (req, res) => {
    models.User.findById(req.params.id)
        .then(user => {
            if (user) {
                user.destroy().then(user => {
                    sendJSONResponse(res, 200, user);
                }).catch(err => {
                    sendJSONResponse(res, 400, err.errors);
                });
            } else {
                sendJSONResponse(res, 404, [{message: "user not found"}]);
            }
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });
};

module.exports.getUser = (req, res) => {
    models.User.findById(req.params.id, {
        include: [{model: models.Fanfic, include: [models.Chapter, models.Tag]}]
    })
        .then(user => {
            if (user) {
                user.password = undefined;
                sendJSONResponse(res, 200, user);
            } else {
                sendJSONResponse(res, 404, [{message: "user not found"}]);
            }
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });
};

module.exports.block = (req, res) => {
    models.User
        .findById(req.params.id)
        .then(user => {
            if (user) {
                user.update({
                    blocked: req.body.blocked
                });
                sendJSONResponse(res, 200, user);
            } else {
                sendJSONResponse(res, 404, [{message: "user not found"}]);
            }
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });
};

module.exports.admin = (req, res) => {
    models.User.findById(req.params.id)
        .then(user => {
            if (user) {
                user.update({
                    admin: req.body.admin
                });
                sendJSONResponse(res, 200, user);
            } else {
                sendJSONResponse(res, 404, [{message: "user not found"}]);
            }
        })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        });
};

module.exports.sign_in = (req, res) => {
    models.User.findOne({
        where: {email: req.body.username}
    }).then(user => {
        if (user && !user.blocked && user.verified_email) {
            verificateUser(req, res, user);
            return;
        }

        if (!user) {
            sendJSONResponse(res, 404, [{message: "user not found"}]);
            return;
        }

        if (user.blocked) {
            sendJSONResponse(res, 401, [{message: "You are blocked!"}]);
            return;
        }
        if (!user.verified_email) {
            sendJSONResponse(res, 401, [{message: "Email not verified!"}]);
        }
    }).catch(err => {
        sendJSONResponse(res, 400, err.errors);
    });
};