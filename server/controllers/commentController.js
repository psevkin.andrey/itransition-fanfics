const sendJSONResponse = require('./utils');
const models = require('../models');
const jwt = require('jsonwebtoken');
const config = require('../config/config.json');

const decodeUserId = (req) => {
    let decoded = req.header('authorization') &&
        req.header('authorization').split(' ')[1] &&
        jwt.verify(req.header('authorization').split(' ')[1], config.jwt.secret);
    return decoded && decoded.user && decoded.user.id;

};

module.exports.createComment = (req, res) => {
    let userId = decodeUserId(req);
    models.Comment.create({
        title: req.body.title,
        content: req.body.content,
        UserId: userId,
        FanficId: req.body.FanficId,
        likes: 0
    }).then(comment => {
        sendJSONResponse(res, 201, comment);
    }).catch(err => {
        sendJSONResponse(res, 400, err.errors);
    });
};

module.exports.deleteComment = (req, res) => {
    let userId = decodeUserId(req);
    let commentId = req.params.id;

    models.Comment.findById(commentId).then(comment => {
        if (comment && comment.UserId === userId) {
            comment.destroy().then(info => {
                sendJSONResponse(res, 200, 'Comment deleted successfully');
            });
        }

        sendJSONResponse(res, 401, 'You haven\'t permissions!');
    }).catch(err => {
        sendJSONResponse(res, 400, err.errors);
    });
};

module.exports.getComments = (req, res) => {
    models.Comment.findAll().then(comments => {
        sendJSONResponse(res, 200, comments);
    }).catch(err => {
        sendJSONResponse(res, 400, err.errors);
    });
};

module.exports.likeComment = (req, res) => {
    models.Like.findOrCreate({
        where: {
            UserId: req.body.UserId,
            CommentId: req.body.CommentId
        },
        defaults: {
            UserId: req.body.UserId,
            CommentId: req.body.CommentId
        }
    }).then(like => {
        if (like[1]) {
            models.Comment.findById(like[0].CommentId).then(comment => {
                if (comment) {
                    comment.update({likes: ++comment.likes}).then(info => {
                        sendJSONResponse(res, 200, 'Comment liked');
                    });
                }
            })
        }else {
            sendJSONResponse(res, 400, 'Comment already liked');
        }
    });
};

module.exports.unlikeComment = (req, res) => {
    models.Like.find({
        where: {
            UserId: req.body.UserId,
            CommentId: req.body.CommentId
        }
    }).then(like => {
        if (like) {
            models.Comment.findById(like.CommentId).then(comment => {
                comment.update({likes: --comment.likes}).then(info => {
                    sendJSONResponse(res, 200, 'Comment unliked.');
                    like.destroy();
                }).catch(err => sendJSONResponse(res, 400, err.errors));
            })
        } else {
            sendJSONResponse(res, 400, 'You didn\'t like this comment');
        }
    }).catch(err => sendJSONResponse(res, 400, err.errors));
};

module.exports.getLikes = (req, res) => {
    models.Like.findAll({where: {UserId: req.query.UserId}}).then(likes => {
        sendJSONResponse(res, 200, likes);
    }).catch(err => {
        sendJSONResponse(res, 400, err.errors);
    })
};