const sendJSONResponse = require('./utils');
const config = require('../config/config.json');
const jwt = require('jsonwebtoken');


module.exports.sendToken = (req, res) => {
    req.user.password = undefined;
    sendJSONResponse(res, 200,
        {
            token: jwt.sign({user: req.user}, config.jwt.secret, {
                expiresIn: '12h',
                audience: config.jwt.audience,
                issuer: config.jwt.issuer
            })
        }
    );
};

module.exports.sendTwitterToken = (req, res) => {
    req.user.password = undefined;
    const token = jwt.sign({user: req.user}, config.jwt.secret, {
                            expiresIn: '12h',
                            audience: config.jwt.audience,
                            issuer: config.jwt.issuer
                        });
    res.writeHead(301,
        {Location: `http://localhost:4200/twitter?token=${token}`}
    );
    res.end();
};