const FacebookStrategy = require('passport-facebook').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;
const VkontakteStrategy = require('passport-vkontakte').Strategy;

const configAuth = require('./config.json');
const models = require('../models');

module.exports = function (passport) {

    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    passport.use(new FacebookStrategy({

            clientID: configAuth.facebookAuth.clientID,
            clientSecret: configAuth.facebookAuth.clientSecret,
            callbackURL: configAuth.facebookAuth.callbackURL,
            profileFields: configAuth.facebookAuth.profileFields

        },

        function (token, refreshToken, profile, done) {

            process.nextTick(function () {
                models.User.findOrCreate({
                    where: {email: `facebook|${profile.emails[0].value}`},
                    defaults: {
                        email: `facebook|${profile.emails[0].value}`,
                        username: profile.name.givenName + ' ' + profile.name.familyName,
                        password: token,
                        image: profile.photos[0].value,
                        verified_email: true,
                        blocked: false,
                        admin: false
                    }
                }).then(user => {
                    user.password = undefined;
                    done(null, user[0]);
                }).catch(err => {
                    done(err);
                });
            });

        }));

    passport.use(new TwitterStrategy({

            consumerKey: configAuth.twitterAuth.consumerKey,
            consumerSecret: configAuth.twitterAuth.consumerSecret,
            callbackURL: configAuth.twitterAuth.callbackURL,
            includeEmail: true


        },

        function (token, refreshToken, profile, done) {

            process.nextTick(function () {
                models.User.findOrCreate({
                    where: {email: `twitter|${profile.emails[0].value}`},
                    defaults: {
                        email: `twitter|${profile.emails[0].value}`,
                        username: profile.username,
                        password: token,
                        image: profile.photos[0].value,
                        verified_email: true,
                        blocked: false,
                        admin: false
                    }
                }).then(user => {
                    done(null, user[0]);
                }).catch(err => {
                    done(err);
                });
            });

        }));


    passport.use(new VkontakteStrategy({

            clientID: configAuth.vkontakteAuth.clientID,
            clientSecret: configAuth.vkontakteAuth.clientSecret,
            callbackURL: configAuth.vkontakteAuth.callbackURL


        },

        function (token, refreshToken, profile, done) {

            process.nextTick(function () {
                models.User.findOrCreate({
                    where: {email: `vkontakte|${profile.id}`},
                    defaults: {
                        email: `vkontakte|${profile.id}`,
                        username: profile.displayName,
                        password: token,
                        image: profile.photos[0].value,
                        verified_email: true,
                        blocked: false,
                        admin: false
                    }
                }).then(user => {
                    done(null, user[0]);
                }).catch(err => {
                    done(err);
                });
            });

        }));

};