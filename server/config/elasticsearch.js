const elasticsearch = require('elasticsearch');

const client = elasticsearch.Client({
    host: 'localhost:9200',
    log: 'info'
});

const indexName = 'fanfics';

const indexExists = () => {
    return client.indices.exists({
        index: indexName
    });
};

module.exports.indexExists = indexExists;

const initIndex = () => {
    return client.indices.create({
        index: indexName
    });
};

module.exports.initIndex = initIndex;

const deleteIndex = () => {
    return client.indices.delete({
        index: indexName
    });
};

module.exports.deleteIndex = deleteIndex;

const initMapping = () => {
    return client.indices.putMapping({
        index: indexName,
        type: "fanfic",
        body: {
            properties: {
                id: {type: 'integer'},
                rating: {type: 'float'},
                createdAt: {type: 'date'},
                title: {type: 'text'},
                description: {type: 'text'},
                genre: {type: 'text'},
                Chapters: [
                    {
                        properties: {
                            title: {type: 'text'},
                            content: {type: 'text'}
                        }
                    }
                ],
                Comments: [
                    {
                        properties: {
                            title: {type: 'text'},
                            content: {type: 'text'}
                        }
                    }
                ],
                Tags: [
                    {
                        properties: {
                            value: {type: 'text'}
                        }
                    }
                ]
            }
        }
    });
};

module.exports.initMapping = initMapping;

const addFanfic = (fanfic) => {
    return client.index({
        index: indexName,
        type: 'fanfic',
        body: {
            id: fanfic.id,
            title: fanfic.title,
            rating: fanfic.rating,
            description: fanfic.description,
            genre: fanfic.genre,
            Chapters: fanfic.Chapters,
            Comments: fanfic.Comments,
            Tags: fanfic.Tags,
            createdAt: fanfic.createdAt
        }
    });
};

module.exports.addFanfic = addFanfic;

const getSuggestions = (input) => {
    return client.search({
        index: indexName,
        body: {
            query: {
                multi_match: {
                    query: input,
                    fields: [
                        'title',
                        'description',
                        'genre',
                        'Chapters.title',
                        'Chapters.content',
                        'Comments.title',
                        'Comments.content',
                        'Tags.value',
                    ]
                }
            },
        }
    });
};

module.exports.getSuggestions = getSuggestions;