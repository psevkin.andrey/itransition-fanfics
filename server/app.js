const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');

const index = require('./routes/index');
const users = require('./routes/users');
const fanfics = require('./routes/fanfics');
const comments = require('./routes/comments');
const passport = require('passport');
const cors = require('cors');
const WebSocketServer = require('ws').Server;
const wss = new WebSocketServer({port: 40510});
const elastic = require('./config/elasticsearch');
const schedule = require('node-schedule');
const models = require('./models');


const app = express();

wss.on('connection', (ws) => {
    ws.on('message', (message) => {
        wss.clients
            .forEach(client => {
                if (client !== ws) {
                    client.send(message);
                }
            });
    });

    ws.on('error', (err) => {
        console.warn(`Client disconnected - reason: ${err}`);
    })
});

require('./config/passport')(passport);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(cors());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', "GET, PUT, POST, DELETE");
    res.header('Access-Control-Allow-Headers', "Content-Type");
    res.header("Content-Type", 'application/json');
    next();
});
app.use(session({
    secret: "SECRET",
    saveUninitialized: true,
    resave: false
}));

app.use('/api', index);
app.use('/api/users', users);
app.use('/api/fanfics', fanfics);
app.use('/api/comments', comments);

elastic.indexExists().then((exists) => {
    if (exists) {
        return elastic.deleteIndex();
    }
}).then(elastic.initIndex)
    .then(elastic.initMapping)
    .catch(e => {})
    .then(() => {
        return models.Fanfic.findAll({
            include: [models.Tag, models.Chapter, models.Comment]
        })
    })
    .then(fanfics => Promise.all(fanfics.map(elastic.addFanfic))).catch(err => {});

schedule.scheduleJob('*/5 * * * *', () => {
    elastic.indexExists().then((exists) => {
        if (exists) {
            return elastic.deleteIndex();
        }
    }).then(elastic.initIndex)
        .then(elastic.initMapping)
        .catch(e => {})
        .then(() => {
            return models.Fanfic.findAll({
                include: [models.Tag, models.Chapter, models.Comment]
            })
        })
        .then(fanfics => Promise.all(fanfics.map(elastic.addFanfic))).catch(err => {});
});

app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
