import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {RegisterComponent} from './components/auth/register/register.component';
import {LoginComponent} from './components/auth/login/login.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {HomeComponent} from './components/home/home.component';
import {FacebookComponent} from './components/auth/social/facebook/facebook.component';
import {TwitterComponent} from './components/auth/social/twitter/twitter.component';
import {VkontakteComponent} from './components/auth/social/vkontakte/vkontakte.component';

import {RoutesModule} from './routes/app.routes';


import {ApiService} from './services/api.service';
import {UserService} from './services/user.service';

import {AUTH_PROVIDERS} from 'angular2-jwt';
import {AuthService} from './services/auth.service';
import {JwtInterceptor} from './helpers/jwt.interceptor';
import {AlertComponent} from './components/alert/alert.component';
import {AlertService} from './services/alert.service';
import {ProfileComponent} from './components/profile/profile.component';
import {AuthGuard} from './guards/auth.guard';
import {AdminGuard} from './guards/admin.guard';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FanficCreateComponent} from './components/fanfics/fanfic-create/fanfic-create.component';
import {FanficService} from './services/fanfic.service';
import {ChapterCreateComponent} from './components/fanfics/chapter-create/chapter-create.component';
import {QuillModule} from 'ngx-quill';
import {TagInputModule} from 'ngx-chips';
import {FileUploadModule} from 'primeng/primeng';
import {ImageUploaderComponent} from './components/image-uploader/image-uploader.component';
import {FanficUpdateComponent} from './components/fanfics/fanfic-update/fanfic-update.component';
import {UpdateFanficGuard} from './guards/update-fanfic.guard';
import {FanficGetComponent} from './components/fanfics/fanfic-get/fanfic-get.component';
import {ShowChapterComponent} from './components/fanfics/show-chapter/show-chapter.component';
import {CreateCommentComponent} from './components/comments/create-comment/create-comment.component';
import {RateChapterComponent} from './components/fanfics/rate-chapter/rate-chapter.component';
import {ShowCommentComponent} from './components/fanfics/show-comment/show-comment.component';
import {StarRatingComponent} from './components/star-rating/star-rating.component';
import {RatingModule} from 'primeng/rating';
import {FanficFormComponent} from './components/fanfics/fanfic-form/fanfic-form.component';
import {DashboardMainComponent} from './components/dashboard/dashboard-main/dashboard-main.component';
import {UserProfileComponent} from './components/dashboard/user-profile/user-profile.component';
import {DashboardCreateFanficComponent} from './components/dashboard/dashboard-create-fanfic/dashboard-create-fanfic.component';
import {UserProfileFormComponent} from './components/user-profile-form/user-profile-form.component';
import {ProfileFanficListComponent} from './components/profile-fanfic-list/profile-fanfic-list.component';
import {DashboardUserCreateComponent} from './components/dashboard/dashboard-user-create/dashboard-user-create.component';
import {HeadComponent} from './components/head/head.component';
import {ModuleWithLocalization} from './config/localization.config';
import {L10nLoader} from 'angular-l10n';
import {ThemeService} from './services/theme.service';
import { SearchComponent } from './components/fanfics/search/search.component';
import { NewestFanficsComponent } from './components/fanfics/newest-fanfics/newest-fanfics.component';
import { PopularFanficsComponent } from './components/fanfics/popular-fanfics/popular-fanfics.component';
import { FanficPreviewComponent } from './components/fanfics/fanfic-preview/fanfic-preview.component';
import {SearchService} from './services/search.service';
import { GetFanficByTagComponent } from './components/fanfics/get-fanfic-by-tag/get-fanfic-by-tag.component';
import { GetFanficByCategoryComponent } from './components/fanfics/get-fanfic-by-category/get-fanfic-by-category.component';
import { CategoriesWidgetComponent } from './components/categories-widget/categories-widget.component';
import { TagsWidgetComponent } from './components/tags-widget/tags-widget.component';
import {ScrollToModule} from 'ng2-scroll-to';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    NavbarComponent,
    HomeComponent,
    FacebookComponent,
    TwitterComponent,
    VkontakteComponent,
    AlertComponent,
    ProfileComponent,
    FanficCreateComponent,
    ChapterCreateComponent,
    ImageUploaderComponent,
    FanficUpdateComponent,
    FanficGetComponent,
    ShowChapterComponent,
    CreateCommentComponent,
    RateChapterComponent,
    ShowCommentComponent,
    StarRatingComponent,
    FanficFormComponent,
    DashboardMainComponent,
    UserProfileComponent,
    DashboardCreateFanficComponent,
    UserProfileFormComponent,
    ProfileFanficListComponent,
    DashboardUserCreateComponent,
    HeadComponent,
    SearchComponent,
    NewestFanficsComponent,
    PopularFanficsComponent,
    FanficPreviewComponent,
    GetFanficByTagComponent,
    GetFanficByCategoryComponent,
    CategoriesWidgetComponent,
    TagsWidgetComponent
  ],
  imports: [
    BrowserModule,
    RoutesModule,
    FormsModule,
    HttpClientModule,
    TableModule,
    BrowserAnimationsModule,
    QuillModule,
    TagInputModule,
    FileUploadModule,
    RatingModule,
    ModuleWithLocalization,
    ScrollToModule.forRoot(),
  ],
  providers: [
    ApiService,
    UserService,
    AuthService,
    AlertService,
    AUTH_PROVIDERS,
    AuthGuard,
    AdminGuard,
    UpdateFanficGuard,
    FanficService,
    ThemeService,
    SearchService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent, HeadComponent]
})
export class AppModule {
  constructor(public l10nLoader: L10nLoader) {
    this.l10nLoader.load();
  }
}
