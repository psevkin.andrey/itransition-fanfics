import { Component, OnInit } from '@angular/core';
import {GenresModel} from '../../models/genres.model';
import {TagModel} from 'ngx-chips/core/accessor';
import {FanficService} from '../../services/fanfic.service';
import {Language} from 'angular-l10n';
import {FanficModel} from '../../models/fanfic.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @Language() lang: string;
  private genres = GenresModel.genres;
  private tags: TagModel[];
  private popularFanfics: FanficModel[];
  private newestFanfics: FanficModel[];

  constructor(private fanficService: FanficService) { }

  ngOnInit() {
    this.fanficService.getTags().subscribe(tags => {
      this.tags = tags;
    });
    this.fanficService.getPopular(5).subscribe(fanfics => {
      this.popularFanfics = fanfics;
    });
    this.fanficService.getNewest(5).subscribe(fanfics => {
      this.newestFanfics = fanfics;
    });
  }
}
