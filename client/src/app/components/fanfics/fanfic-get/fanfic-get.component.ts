import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FanficService} from '../../../services/fanfic.service';
import {FanficModel} from '../../../models/fanfic.model';
import {UserModel} from '../../../models/user.model';
import {AuthService} from '../../../services/auth.service';
import {ApiService} from '../../../services/api.service';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-fanfic-get',
  templateUrl: './fanfic-get.component.html',
  styleUrls: ['./fanfic-get.component.css']
})
export class FanficGetComponent implements OnInit {

  @Language() lang: string;
  private visible: boolean;
  private visibleChapters: boolean;
  private fanfic: FanficModel;
  public likes: any[];
  public rates: any[];
  private user: UserModel;
  private currentPage;
  private comments: any[];

  constructor(private route: ActivatedRoute,
              private auth: AuthService,
              private apiService: ApiService,
              private fanficService: FanficService) { }

  ngOnInit() {
    this.fanficService.getFanfic(+this.route.snapshot.paramMap.get('id')).subscribe(fanfic => {
      this.fanfic = fanfic;
      this.fanfic.Chapters = this.fanfic.Chapters.sort((el1, el2) => el1.number - el2.number);
      this.user = fanfic.User;
    });

    this.apiService.getLikes(this.auth.user.id).subscribe(likes => {
      this.likes = likes;
    });

    if (this.auth.authenticated) {
      this.fanficService.getAllRates(this.auth.user.id).subscribe(rates => {
        this.rates = rates;
      });
    }

    this.visible = true;
    this.visibleChapters = false;
  }

  setPage(page: number) {
    if (page <= this.fanfic.Chapters.length) {
      this.currentPage = page - 1;
    }
  }

  liked(id: number): boolean {
    return this.likes && this.likes.findIndex(el => el.CommentId === id) >= 0;
  }

  switchMode() {
    this.visible = !this.visible;
  }

  showChapters() {
    this.visibleChapters = !this.visibleChapters;
  }
}
