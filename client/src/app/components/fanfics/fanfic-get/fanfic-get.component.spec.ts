import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FanficGetComponent } from './fanfic-get.component';

describe('FanficGetComponent', () => {
  let component: FanficGetComponent;
  let fixture: ComponentFixture<FanficGetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FanficGetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FanficGetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
