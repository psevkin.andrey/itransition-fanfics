import {Component, Input, OnInit, Output} from '@angular/core';
import {ChapterModel} from '../../../models/chapter.model';
import {FanficModel} from '../../../models/fanfic.model';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-chapter-create',
  templateUrl: './chapter-create.component.html',
  styleUrls: ['./chapter-create.component.css']
})
export class ChapterCreateComponent implements OnInit {

  @Language() lang: string;
  private closed = true;

  @Input() chapter: ChapterModel;

  constructor() {
  }

  ngOnInit() {
  }

  getInfo() {
    this.closed = !this.closed;
  }
}
