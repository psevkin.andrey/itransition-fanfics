import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FanficUpdateComponent } from './fanfic-update.component';

describe('FanficUpdateComponent', () => {
  let component: FanficUpdateComponent;
  let fixture: ComponentFixture<FanficUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FanficUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FanficUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
