import {Component, OnInit} from '@angular/core';
import {FanficService} from '../../../services/fanfic.service';
import {AlertService} from '../../../services/alert.service';
import {FanficModel} from '../../../models/fanfic.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {Language, TranslationService} from 'angular-l10n';

@Component({
  selector: 'app-fanfic-update',
  templateUrl: './fanfic-update.component.html',
  styleUrls: ['./fanfic-update.component.css']
})
export class FanficUpdateComponent implements OnInit {

  @Language() lang: string;
  private fanfic: FanficModel;

  constructor(private fanficService: FanficService,
              private alert: AlertService,
              private location: Location,
              private route: ActivatedRoute,
              private translate: TranslationService) {
  }

  ngOnInit() {
    this.fanficService.getFanfic(+this.route.snapshot.paramMap.get('id')).subscribe(fanfic => {
      this.fanfic = fanfic;
    });
  }

  onSubmit(event) {
    this.fanficService.updateFanfic(event.formData, this.fanfic.id)
      .subscribe((info: string) => {
        this.alert.success(this.translate.translate('Fanfics.alert.success.successUpdate'), true);
        this.location.back();
      }, err => {
        this.alert.error(this.translate.translate('Fanfics.alert.errors.wentWrong'));
      });
  }

}
