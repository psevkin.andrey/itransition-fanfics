import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../../../services/api.service';
import {FanficModel} from '../../../models/fanfic.model';
import {Language} from 'angular-l10n';
import {SearchService} from '../../../services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Language() lang: string;
  private fanfics: FanficModel[];

  constructor(private route: ActivatedRoute,
              private apiService: ApiService,
              private searchService: SearchService) { }

  ngOnInit() {
    if (this.searchService.searchQuery) {
      this.apiService.search(this.searchService.searchQuery).subscribe(results => {
        this.fanfics = results;
      });
    }
  }

}
