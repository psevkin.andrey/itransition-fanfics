import { Component, OnInit } from '@angular/core';
import {FanficService} from '../../../services/fanfic.service';
import {FanficModel} from '../../../models/fanfic.model';
import {ActivatedRoute} from '@angular/router';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-get-fanfic-by-tag',
  templateUrl: './get-fanfic-by-tag.component.html',
  styleUrls: ['./get-fanfic-by-tag.component.css']
})
export class GetFanficByTagComponent implements OnInit {

  private fanfics: FanficModel[];
  @Language() lang: string;

  constructor(private fanficService: FanficService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const tag = this.route.snapshot.params.tag;
    this.fanficService.getFanficsByTag(tag)
      .subscribe(fanfics => {
        console.log(fanfics);
        this.fanfics = fanfics;
    });
  }

}
