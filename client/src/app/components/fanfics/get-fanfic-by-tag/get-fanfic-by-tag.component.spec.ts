import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetFanficByTagComponent } from './get-fanfic-by-tag.component';

describe('GetFanficByTagComponent', () => {
  let component: GetFanficByTagComponent;
  let fixture: ComponentFixture<GetFanficByTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetFanficByTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetFanficByTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
