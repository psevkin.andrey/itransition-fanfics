import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewestFanficsComponent } from './newest-fanfics.component';

describe('NewestFanficsComponent', () => {
  let component: NewestFanficsComponent;
  let fixture: ComponentFixture<NewestFanficsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewestFanficsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewestFanficsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
