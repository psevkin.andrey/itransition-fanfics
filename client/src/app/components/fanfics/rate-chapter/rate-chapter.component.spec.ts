import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateChapterComponent } from './rate-chapter.component';

describe('RateChapterComponent', () => {
  let component: RateChapterComponent;
  let fixture: ComponentFixture<RateChapterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateChapterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateChapterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
