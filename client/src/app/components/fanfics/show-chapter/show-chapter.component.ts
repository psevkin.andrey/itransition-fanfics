import {Component, Input, OnInit} from '@angular/core';
import {ChapterModel} from '../../../models/chapter.model';
import {FanficService} from '../../../services/fanfic.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-show-chapter',
  templateUrl: './show-chapter.component.html',
  styleUrls: ['./show-chapter.component.css']
})
export class ShowChapterComponent implements OnInit {

  @Input() chapter: ChapterModel;
  @Input() rates: any[];

  private rate: any;

  constructor(private fanficService: FanficService,
              private auth: AuthService) { }

  ngOnInit() {
    const index = this.rates && this.rates.findIndex(el => el.ChapterId === this.chapter.id);
    if (index >= 0) {
      this.rate = this.rates[index].rating;
    }
  }

  isRated(): boolean {
    return this.rates && this.rates.findIndex(el => el.ChapterId === this.chapter.id) >= 0;
  }

  rateIt(event) {
    this.fanficService.rateChapter({
      UserId: this.auth.user.id,
      ChapterId: this.chapter.id,
      rating: event.value
    }).subscribe(info => {
      this.chapter.rating += event.value;
      this.chapter.qty++;
      console.log(info);
    });
  }

  getRating() {
    return this.chapter.qty === 0 ? '0.0' : (this.chapter.rating / this.chapter.qty).toFixed(1);
  }
}
