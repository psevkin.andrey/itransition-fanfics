import {Component, Input, OnInit} from '@angular/core';
import {FanficModel} from '../../../models/fanfic.model';
import {FanficService} from '../../../services/fanfic.service';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';
import {Location} from '@angular/common';
import {Language, TranslationService} from 'angular-l10n';

@Component({
  selector: 'app-fanfic-create',
  templateUrl: './fanfic-create.component.html',
  styleUrls: ['./fanfic-create.component.css']
})
export class FanficCreateComponent implements OnInit {

  @Language() lang: string;
  private fanfic: FanficModel;

  constructor(private fanficService: FanficService,
              private location: Location,
              private alert: AlertService,
              private auth: AuthService,
              private translate: TranslationService) {
  }

  ngOnInit() {
    this.fanfic = new FanficModel();
  }

  onSubmit(event) {
    this.fanficService.createFanfic(event.formData).subscribe(info => {
      this.auth.fanfics.push(info);
      this.alert.success(this.translate.translate('Fanfics.alert.success.successCreate'), true);
      this.location.back();
    }, err => {
      console.log(err);
      this.alert.error(this.translate.translate('Fanfics.alert.errors.wentWrong'));
    });
  }
}
