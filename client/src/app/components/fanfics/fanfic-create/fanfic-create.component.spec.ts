import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FanficCreateComponent } from './fanfic-create.component';

describe('FanficCreateComponent', () => {
  let component: FanficCreateComponent;
  let fixture: ComponentFixture<FanficCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FanficCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FanficCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
