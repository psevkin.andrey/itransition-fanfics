import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetFanficByCategoryComponent } from './get-fanfic-by-category.component';

describe('GetFanficByCategoryComponent', () => {
  let component: GetFanficByCategoryComponent;
  let fixture: ComponentFixture<GetFanficByCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetFanficByCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetFanficByCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
