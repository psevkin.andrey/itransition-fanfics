import { Component, OnInit } from '@angular/core';
import {FanficModel} from '../../../models/fanfic.model';
import {FanficService} from '../../../services/fanfic.service';
import {ActivatedRoute} from '@angular/router';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-get-fanfic-by-category',
  templateUrl: './get-fanfic-by-category.component.html',
  styleUrls: ['./get-fanfic-by-category.component.css']
})
export class GetFanficByCategoryComponent implements OnInit {

  private fanfics: FanficModel[];
  @Language() lang: string;

  constructor(private fanficService: FanficService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.fanficService.getFanficsByCategory(this.route.snapshot.params.genre).subscribe(fanfics => {
      this.fanfics = fanfics;
    });
  }

}
