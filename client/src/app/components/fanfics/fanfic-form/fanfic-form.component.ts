import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FanficModel} from '../../../models/fanfic.model';
import {AuthService} from '../../../services/auth.service';
import {ChapterModel} from '../../../models/chapter.model';
import {GenresModel} from '../../../models/genres.model';
import {TagModel} from 'ngx-chips/core/accessor';
import {FanficService} from '../../../services/fanfic.service';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-fanfic-form',
  templateUrl: './fanfic-form.component.html',
  styleUrls: ['./fanfic-form.component.css']
})
export class FanficFormComponent implements OnInit {

  @Language() lang: string;
  @Input() fanfic: FanficModel;
  @Input() btnText: string;
  @Output() onSubmit: EventEmitter<any> = new EventEmitter();

  private chapters: ChapterModel[];
  private genres = GenresModel.genres;
  private tags: TagModel[] = [];

  constructor(private auth: AuthService,
              private fanficService: FanficService) {
  }

  ngOnInit() {
    this.fanficService.getTags().subscribe(tags => {
      this.tags = tags;
    }, err => {
      console.log(err.errors);
    });
    this.chapters = this.fanfic.Chapters ?
      this.fanfic.Chapters.sort((el1, el2) => el1.number - el2.number) : [];
  }

  titleValidate(title): string {
    if (title.errors) {
      if (title.errors.required) {
        return 'Fanfics.form.title.required';
      }
    }
  }

  descriptionValidate(description): string {
    if (description.errors) {
      if (description.errors.required) {
        return 'Fanfics.form.description.required';
      }
    }
  }

  deleteChapter(num: number) {
    this.chapters = this.chapters.filter(el => el.number !== num)
      .filter(el => {
        if (el.number > num) {
          el.number--;
        }

        return true;
      });
  }

  pushNewChapter() {
    this.chapters.push(new ChapterModel(this.chapters.length + 1, null, null, null));
  }

  collectChaptersImages(fd: FormData) {
    for (let chapter of this.chapters) {
      if (chapter.image) {
        fd.append(`chapter${chapter.number}`, chapter.image);
      }
    }
  }

  submitForm(formData) {
    let fd = new FormData();
    fd.append('fanfic', this.fanfic.image);
    this.collectChaptersImages(fd);
    this.fanfic.Chapters = this.chapters;
    this.fanfic.UserId = this.fanfic.UserId || this.auth.user.id;
    fd.append('content', JSON.stringify(this.fanfic));
    this.onSubmit.emit({formData: fd});
  }

}
