import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FanficFormComponent } from './fanfic-form.component';

describe('FanficFormComponent', () => {
  let component: FanficFormComponent;
  let fixture: ComponentFixture<FanficFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FanficFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FanficFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
