import {Component, Input, OnInit} from '@angular/core';
import {CommentModel} from '../../../models/comment.model';
import {UserModel} from '../../../models/user.model';
import {UserService} from '../../../services/user.service';
import {ApiService} from '../../../services/api.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-show-comment',
  templateUrl: './show-comment.component.html',
  styleUrls: ['./show-comment.component.css']
})
export class ShowCommentComponent implements OnInit {

  @Input() comment: CommentModel;
  @Input() liked: boolean;
  private user: UserModel;
  private likes: any[];

  constructor(private userService: UserService,
              private auth: AuthService,
              private apiService: ApiService) { }

  ngOnInit() {
    this.userService.getUser(this.comment.UserId).subscribe(userInfo => {
      this.user = userInfo;
    });
  }

  like() {
    this.liked = !this.liked;
    if (this.liked) {
      this.apiService.like(this.auth.user.id, this.comment.id).subscribe(info => {
        console.log(info);
        this.comment.likes++;
      }, err => {
        console.log(err);
      });
    } else {
      this.apiService.unlike(this.auth.user.id, this.comment.id).subscribe(info => {
        console.log(info);
        this.comment.likes--;
      }, err => {
        console.log(err);
      });
    }
  }
}
