import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularFanficsComponent } from './popular-fanfics.component';

describe('PopularFanficsComponent', () => {
  let component: PopularFanficsComponent;
  let fixture: ComponentFixture<PopularFanficsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopularFanficsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularFanficsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
