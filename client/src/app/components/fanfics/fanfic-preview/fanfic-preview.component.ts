import {Component, Input, OnInit} from '@angular/core';
import {FanficModel} from '../../../models/fanfic.model';

@Component({
  selector: 'app-fanfic-preview',
  templateUrl: './fanfic-preview.component.html',
  styleUrls: ['./fanfic-preview.component.css']
})
export class FanficPreviewComponent implements OnInit {

  @Input() fanfic: FanficModel;

  constructor() { }

  ngOnInit() {
  }

}
