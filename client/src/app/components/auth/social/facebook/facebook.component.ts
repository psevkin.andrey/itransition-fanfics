import { Component, OnInit } from '@angular/core';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {AuthService} from '../../../../services/auth.service';


@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.css']
})
export class FacebookComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService) { }

  ngOnInit() {
    const code = this.route.snapshot.queryParams['code'];
    this.auth.callbackSocial('facebook', code)
      .subscribe(() => {},
          err => {
            console.log(err);
          });
    this.router.navigate(['/']);
  }

}
