import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VkontakteComponent } from './vkontakte.component';

describe('VkontakteComponent', () => {
  let component: VkontakteComponent;
  let fixture: ComponentFixture<VkontakteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VkontakteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VkontakteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
