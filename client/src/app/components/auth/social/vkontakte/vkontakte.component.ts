import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../services/auth.service';

@Component({
  selector: 'app-vkontakte',
  templateUrl: './vkontakte.component.html',
  styleUrls: ['./vkontakte.component.css']
})
export class VkontakteComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService) { }

  ngOnInit() {
    const code = this.route.snapshot.queryParams['code'];
    this.auth.callbackSocial('vkontakte', code).subscribe(() => {},
      err => {
        console.log(err);
      });;
    this.router.navigate(['/']);
  }

}
