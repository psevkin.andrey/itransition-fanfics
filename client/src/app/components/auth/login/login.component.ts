import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';
import {Language} from 'angular-l10n';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Language() lang: string;
  model: any = {};
  loading = false;

  constructor(private auth: AuthService,
              private router: Router,
              private alert: AlertService) {
  }

  ngOnInit() {
    if (this.auth.authenticated) {
      this.router.navigate(['/']);
    }
  }


  logout() {
    this.auth.logout();
  }

  onSubmit(formData) {
    if (formData.valid) {
      this.loading = true;
      this.auth.signWithEmail(this.model.email, this.model.password)
        .subscribe(() => {
            if (this.auth.authenticated) {
              this.router.navigate(['/']);
            }
          }, err => {
            this.loading = false;
            this.alert.error(err.error[0].message);
          }
        );
    }
  }

  loginWithFacebook() {
    this.auth.signWithSocial('facebook');
  }

  loginWithTwitter() {
    this.auth.signWithSocial('twitter');
  }

  loginWithVkontakte() {
    this.auth.signWithSocial('vkontakte');
  }

  passwordValidate(password) {
    if (password.errors) {
      if (password.errors.required) {
        return 'Profile.Password.required';
      }

      if (password.errors.minlength) {
        return 'Profile.Password.minlength';
      }
    }


    return null;
  }

  emailValidate(email): string {

    if (email.errors) {
      if (email.errors.required) {
        return 'Profile.Email.required';
      }

      if (email.errors.email) {
        return 'Profile.Email.format';
      }

      if (email.errors.minlength) {
        return 'Profile.Email.minlength';
      }
    }

    return null;
  }
}
