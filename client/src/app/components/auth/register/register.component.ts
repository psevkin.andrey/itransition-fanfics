import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';
import {ApiService} from '../../../services/api.service';
import {AlertService} from '../../../services/alert.service';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Language() lang: string;
  model: any = {};
  loading = false;

  constructor(private auth: UserService,
              private router: Router,
              private userService: UserService,
              private alert: AlertService) {
  }

  ngOnInit() {
  }


  onSubmit(formData) {
    if (formData.valid) {
      this.loading = true;
      let fd = new FormData();
      fd.append('avatar', this.model.image);
      fd.append('content', JSON.stringify(this.model));
      this.userService.registerUser(fd).subscribe(info => {
        this.alert.success(info[0].message, true);
        this.router.navigate(['/']);
      }, err => {
        this.alert.error(err.error[0].message);
        this.loading = false;
      });

    }
  }

  passwordValidate(password) {
    if (password.errors) {
      if (password.errors.required) {
        return 'Profile.Password.required';
      }

      if (password.errors.minlength) {
        return 'Profile.Password.minlength';
      }
    }


    return null;
  }

  emailValidate(email): string {

    if (email.errors) {
      if (email.errors.required) {
        return 'Profile.Email.required';
      }

      if (email.errors.email) {
        return 'Profile.Email.format';
      }

      if (email.errors.minlength) {
        return 'Profile.Email.minlength';
      }
    }

    return null;
  }

  usernameValidate(username): string {

    if (username.errors) {
      if (username.errors.required) {
        return 'Profile.Username.required';
      }

      if (username.errors.minlength) {
        return 'Profile.Username.minlength';
      }
    }

    return null;
  }

  confirmationValidate(confirmation): boolean {
    return this.model.password === this.model.confirmation && (confirmation.dirty || confirmation.touched);
  }
}
