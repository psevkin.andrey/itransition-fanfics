import {Component, Input, OnInit} from '@angular/core';
import {UserModel} from '../../models/user.model';
import {UserService} from '../../services/user.service';
import {AlertService} from '../../services/alert.service';
import {Language, TranslationService} from 'angular-l10n';

@Component({
  selector: 'app-user-profile-form',
  templateUrl: './user-profile-form.component.html',
  styleUrls: ['./user-profile-form.component.css']
})
export class UserProfileFormComponent implements OnInit {

  @Language() lang: string;
  @Input() user: UserModel;

  constructor(private userService: UserService,
              private alert: AlertService,
              public translation: TranslationService) {
  }

  ngOnInit() {
  }

  passwordValidate(password) {
    if (password.errors) {
      if (password.errors.required) {
        return 'Profile.Password.required';
      }

      if (password.errors.minlength) {
        return 'Profile.Password.minlength';
      }
    }


    return null;
  }

  emailValidate(email): string {

    if (email.errors) {
      if (email.errors.required) {
        return 'Profile.Email.required';
      }

      if (email.errors.email) {
        return 'Profile.Email.format';
      }

      if (email.errors.minlength) {
        return 'Profile.Email.minlength';
      }
    }

    return null;
  }

  usernameValidate(username): string {

    if (username.errors) {
      if (username.errors.required) {
        return 'Profile.Username.required';
      }

      if (username.errors.minlength) {
        return 'Profile.Username.minlength';
      }
    }

    return null;
  }

  confirmationValidate(confirmation): boolean {
    return this.user.password === confirmation.value && (confirmation.dirty || confirmation.touched);
  }

  onSubmit(formData) {
    let fd = new FormData();
    fd.append('avatar', this.user.image);
    fd.append('content', JSON.stringify(this.user));
    if (formData.valid) {
      this.userService.updateUser(fd, this.user.id).subscribe(message => {
        this.alert.success(this.translation.translate('Profile.alert.userUpdated'));
      }, err => {
        this.alert.error(err.error[0].message);
      });
    }
  }

}
