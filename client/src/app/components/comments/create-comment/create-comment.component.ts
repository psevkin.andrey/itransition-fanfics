import {Component, Input, OnInit} from '@angular/core';
import {FanficModel} from '../../../models/fanfic.model';
import {CommentModel} from '../../../models/comment.model';
import {AuthService} from '../../../services/auth.service';
import {ApiService} from '../../../services/api.service';
import {WebSocketSubject} from 'rxjs/observable/dom/WebSocketSubject';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.css']
})
export class CreateCommentComponent implements OnInit {

  @Input() fanfic: FanficModel;

  private comment: CommentModel;
  private socket$: WebSocketSubject<CommentModel>;

  constructor(private auth: AuthService, private apiService: ApiService) {
  }

  ngOnInit() {
    this.socket$ = WebSocketSubject.create('ws://localhost:40510/');
    this.comment = new CommentModel();

    this.socket$.subscribe(comment => {
      this.fanfic.Comments.push(comment);
    }, err => console.error(err));
  }

  onSubmit() {
    this.apiService.createComment({
      title: this.comment.title,
      content: this.comment.content,
      FanficId: this.fanfic.id
    }).subscribe(comment => {
      this.fanfic.Comments.push(comment);
      this.socket$.next(<any>JSON.stringify(comment));
      this.comment = new CommentModel();
    });
  }

  titleValidate(title) {
    if (title.errors) {
      if (title.errors.required) {
        return 'Title is required.';
      }

      if (title.errors.minlength) {
        return 'Title must be at least 5 characters.';
      }
    }
  }

}
