import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {FanficModel} from '../../../models/fanfic.model';
import {UserService} from '../../../services/user.service';
import {UserModel} from '../../../models/user.model';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  @Language() lang: string;
  private userId: number;
  private user: UserModel;
  private fanfics: FanficModel[];
  private createURL: string;

  constructor(private userService: UserService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.userId = +this.route.snapshot.paramMap.get('id');
    this.createURL = `dashboard/user/${this.userId}/fanfic/create`;
    this.fanfics = [];
    this.userService.getUser(this.userId).subscribe(user => {
      this.fanfics = user.Fanfics;
      user.Fanfics = null;
      this.user = user;
    });
  }
}
