import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardUserCreateComponent } from './dashboard-user-create.component';

describe('DashboardUserCreateComponent', () => {
  let component: DashboardUserCreateComponent;
  let fixture: ComponentFixture<DashboardUserCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardUserCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardUserCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
