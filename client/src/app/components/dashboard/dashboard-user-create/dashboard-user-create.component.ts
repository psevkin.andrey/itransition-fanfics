import { Component, OnInit } from '@angular/core';
import {UserModel} from '../../../models/user.model';
import {UserService} from '../../../services/user.service';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-dashboard-user-create',
  templateUrl: './dashboard-user-create.component.html',
  styleUrls: ['./dashboard-user-create.component.css']
})
export class DashboardUserCreateComponent implements OnInit {

  @Language() lang: string;
  private model: UserModel;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.model = {} as UserModel;
  }

  onSubmit(f) {
    let fd = new FormData();
    fd.append('avatar', this.model.image);
    fd.append('content', JSON.stringify(this.model));
    this.userService.registerUser(fd).subscribe(info => {
      console.log(info);
    });
  }

  passwordValidate(password) {
    if (password.errors) {
      if (password.errors.required) {
        return 'Profile.Password.required';
      }

      if (password.errors.minlength) {
        return 'Profile.Password.minlength';
      }
    }


    return null;
  }

  emailValidate(email): string {

    if (email.errors) {
      if (email.errors.required) {
        return 'Profile.Email.required';
      }

      if (email.errors.email) {
        return 'Profile.Email.format';
      }

      if (email.errors.minlength) {
        return 'Profile.Email.minlength';
      }
    }

    return null;
  }

  usernameValidate(username): string {

    if (username.errors) {
      if (username.errors.required) {
        return 'Profile.Username.required';
      }

      if (username.errors.minlength) {
        return 'Profile.Username.minlength';
      }
    }

    return null;
  }

  confirmationValidate(confirmation): boolean {
    return this.model.password === confirmation.value && (confirmation.dirty || confirmation.touched);
  }

}
