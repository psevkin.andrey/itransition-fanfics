import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user.service';
import {UserModel} from '../../../models/user.model';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {FanficService} from '../../../services/fanfic.service';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.css']
})
export class DashboardMainComponent implements OnInit {

  @Language() lang: string;
  private users: UserModel[];
  private selectedUsers: UserModel[];
  private cols: any[];

  constructor(private userService: UserService,
              private fanficService: FanficService,
              private router: Router,
              private auth: AuthService) {
  }

  ngOnInit() {
    this.users = [];
    this.userService.getUsers().subscribe(users => {
      this.users = users;
    });

    this.cols = [
      {field: 'username', header: 'Dashboard.tableHeaders.Username'},
      {field: 'email', header: 'Dashboard.tableHeaders.Email'},
    ];
  }

  selectUserWithButton(user: UserModel) {
    this.router.navigate([`/dashboard/user/${user.id}`]);
  }

  isDiamondBtnDisable() {
    return !this.selectedUsers || this.selectedUsers.length === 0 || this.selectedUsers.find(el => el.admin);
  }

  isWheelchairBtnDisable() {
    return !this.selectedUsers || this.selectedUsers.length === 0 || this.selectedUsers.find(el => !el.admin);
  }

  isUnblockBtnDisable() {
    return !this.selectedUsers || this.selectedUsers.length === 0 || this.selectedUsers.find(el => !el.blocked);
  }

  isBlockBtnDisable() {
    return !this.selectedUsers || this.selectedUsers.length === 0 || this.selectedUsers.find(el => el.blocked);
  }

  isDeleteBtnDisable() {
    return !this.selectedUsers || this.selectedUsers.length === 0;
  }

  blockUsers() {
    for (let user of this.selectedUsers) {
      this.userService.blockUser(user.id, true).subscribe(info => {
        console.log(info);
        user.blocked = true;
      });
    }

    this.selectedUsers = [];
  }

  unblockUsers() {
    for (let user of this.selectedUsers) {
      this.userService.blockUser(user.id, false).subscribe(info => {
        console.log(info);
        user.blocked = false;
      });
    }

    this.selectedUsers = [];
  }

  givePrivileges() {
    for (let user of this.selectedUsers) {
      this.userService.setAdmin(user.id, true).subscribe(info => {
        console.log(info);
        user.admin = true;
      });
    }

    this.selectedUsers = [];
  }

  takeAwayPrivileges() {
    for (let user of this.selectedUsers) {
      this.userService.setAdmin(user.id, false).subscribe(info => {
        console.log(info);
        user.admin = false;
      });
    }

    this.selectedUsers = [];
  }

  deleteUsers() {
    for (let user of this.selectedUsers) {
      this.userService.deleteUser(user.id).subscribe(info => {
        console.log(info);
        this.users = this.users.filter(el => el.id !== user.id);
      });

    }

    this.selectedUsers = [];
  }

}
