import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {FanficModel} from '../../../models/fanfic.model';
import {FanficService} from '../../../services/fanfic.service';
import {Location} from '@angular/common';
import {AlertService} from '../../../services/alert.service';
import {Language, TranslationService} from 'angular-l10n';

@Component({
  selector: 'app-dashboard-create-fanfic',
  templateUrl: './dashboard-create-fanfic.component.html',
  styleUrls: ['./dashboard-create-fanfic.component.css']
})
export class DashboardCreateFanficComponent implements OnInit {

  @Language() lang: string;
  private UserId: number;
  private fanfic: FanficModel;

  constructor(private fanficService: FanficService,
              private route: ActivatedRoute,
              private location: Location,
              private alert: AlertService,
              private auth: AuthService,
              private translate: TranslationService) {
  }

  ngOnInit() {
    this.UserId = +this.route.snapshot.paramMap.get('id');
    this.fanfic = new FanficModel();
    this.fanfic.UserId = this.UserId;
  }

  onSubmit(event) {
    this.fanficService.createFanfic(event.formData).subscribe(info => {
      this.auth.fanfics.push(info);
      this.alert.success(this.translate.translate('Fanfics.alert.success.successCreate'), true);
      this.location.back();
    }, err => {
      console.log(err);
      this.alert.error(this.translate.translate('Fanfics.alert.errors.wentWrong'));
    });
  }
}
