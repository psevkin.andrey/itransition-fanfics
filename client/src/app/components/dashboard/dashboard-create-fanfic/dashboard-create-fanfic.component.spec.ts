import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCreateFanficComponent } from './dashboard-create-fanfic.component';

describe('DashboardCreateFanficComponent', () => {
  let component: DashboardCreateFanficComponent;
  let fixture: ComponentFixture<DashboardCreateFanficComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardCreateFanficComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardCreateFanficComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
