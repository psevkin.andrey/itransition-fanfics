import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.css']
})
export class ImageUploaderComponent implements OnInit {

  @Input() model: any;
  private file: File;

  constructor() {
  }

  ngOnInit() {
    this.file = this.model.image;
  }

  myUploader(event) {
    this.model.image = event.files[0];
    this.file = event.files[0];
  }

  onRemove(event) {
    this.model.image = null;
    this.file = null;
  }
}
