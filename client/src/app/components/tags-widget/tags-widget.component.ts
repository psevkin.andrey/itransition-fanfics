import {Component, Input, OnInit} from '@angular/core';
import {TagModel} from 'ngx-chips/core/accessor';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-tags-widget',
  templateUrl: './tags-widget.component.html',
  styleUrls: ['./tags-widget.component.css']
})
export class TagsWidgetComponent implements OnInit {

  @Input() tags: TagModel[];
  @Language() lang: string;

  constructor() { }

  ngOnInit() {
  }

}
