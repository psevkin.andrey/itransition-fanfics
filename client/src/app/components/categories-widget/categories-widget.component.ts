import {Component, Input, OnInit} from '@angular/core';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-categories-widget',
  templateUrl: './categories-widget.component.html',
  styleUrls: ['./categories-widget.component.css']
})
export class CategoriesWidgetComponent implements OnInit {

  @Input() genres: string[];
  @Language() lang: string;

  constructor() { }

  ngOnInit() {
  }

}
