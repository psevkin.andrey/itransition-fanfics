import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Language, LocaleService} from 'angular-l10n';
import {ThemeService} from '../../services/theme.service';
import {Router} from '@angular/router';
import {SearchService} from '../../services/search.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Language() lang: string;
  private searchQuery: string;

  constructor(private auth: AuthService,
              public locale: LocaleService,
              public theme: ThemeService,
              private router: Router,
              private searchService: SearchService) {
  }

  ngOnInit() {
  }

  selectLanguage(language: string): void {
    this.locale.setCurrentLanguage(language);
  }

  search() {
    this.router.navigate(['/search'], { queryParams: { search: this.searchQuery } });
    this.searchService.searchQuery = this.searchQuery;
    this.searchQuery = '';
  }

}
