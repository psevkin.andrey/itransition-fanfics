import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../services/theme.service';

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.css']
})
export class HeadComponent implements OnInit {

  private link: string;

  constructor(private theme: ThemeService) { }

  ngOnInit() {
    this.link = 'https://bootswatch.com/4/minty/bootstrap.min.css';
  }

}
