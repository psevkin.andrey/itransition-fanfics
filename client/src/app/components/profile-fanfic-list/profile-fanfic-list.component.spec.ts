import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileFanficListComponent } from './profile-fanfic-list.component';

describe('ProfileFanficListComponent', () => {
  let component: ProfileFanficListComponent;
  let fixture: ComponentFixture<ProfileFanficListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileFanficListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFanficListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
