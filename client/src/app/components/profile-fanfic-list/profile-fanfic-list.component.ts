import {Component, Input, OnInit} from '@angular/core';
import {FanficModel} from '../../models/fanfic.model';
import {FanficService} from '../../services/fanfic.service';
import {Router} from '@angular/router';
import {AlertService} from '../../services/alert.service';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-profile-fanfic-list',
  templateUrl: './profile-fanfic-list.component.html',
  styleUrls: ['./profile-fanfic-list.component.css']
})
export class ProfileFanficListComponent implements OnInit {

  @Language() lang: string;
  @Input() fanfics: FanficModel[];
  @Input() createPageURL: string;
  private cols: any[];
  private selectedFanfics: FanficModel[];

  constructor(private fanficService: FanficService,
              private alert: AlertService,
              private router: Router) {
  }

  ngOnInit() {
    this.cols = [
      {field: 'title', header: 'Profile.columnHeaders.Title'},
      {field: 'genre', header: 'Profile.columnHeaders.Genre'},
      {field: 'createdAt', header: 'Profile.columnHeaders.CreatedAt'}
    ];
  }

  navigateToCreatePage() {
    this.router.navigate([this.createPageURL]);
  }

  deleteFanfics() {
    for (let fanfic of this.selectedFanfics) {
      this.fanficService.delete(fanfic.id).subscribe(item => {
        this.fanfics = this.fanfics.filter(el => el.id !== fanfic.id);
      }, err => {
        this.alert.error(err.error[0].message);
      });
    }
  }

}
