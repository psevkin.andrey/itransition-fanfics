import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {

  private rating = 0;

  constructor() {
  }

  ngOnInit() {
  }

  rate(event) {
    console.log(event.value);
  }
}
