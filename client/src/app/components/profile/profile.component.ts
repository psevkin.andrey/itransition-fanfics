import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {UserService} from '../../services/user.service';
import {UserModel} from '../../models/user.model';
import {AuthService} from '../../services/auth.service';
import {FanficModel} from '../../models/fanfic.model';
import {FanficService} from '../../services/fanfic.service';
import {AlertService} from '../../services/alert.service';
import {Router} from '@angular/router';
import {Language} from 'angular-l10n';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit {

  @Language() lang: string;
  private user: UserModel;
  private fanfics: FanficModel[];

  constructor(private userService: UserService,
              private auth: AuthService) { }

  ngOnInit() {
    this.fanfics = [];
    if (this.auth.user) {
      this.userService.getUser(this.auth.user.id).subscribe(user => {
        this.fanfics = user.Fanfics;
        user.Fanfics = null;
        this.user = user;
      });
    }
  }
}
