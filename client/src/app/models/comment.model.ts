export class CommentModel {

  public id: number;
  public title: string;
  public content: string;
  public likes: number;
  public UserId: number;
  public FanficId: number;

  constructor() {
    this.likes = 0;
  }

}
