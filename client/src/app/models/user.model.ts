export class UserModel {

  constructor(
    public id: number,
    public email: string,
    public username: string,
    public image: string,
    public blocked: boolean,
    public verified_email: boolean,
    public admin: boolean,
    public password = null
  ) {  }

}
