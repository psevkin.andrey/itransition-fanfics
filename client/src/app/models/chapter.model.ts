export class ChapterModel {

  public id: number;
  public rating: number;
  public qty: number;

  constructor(
    public number: number,
    public title: string,
    public content: string,
    public image: string
  ) {  }

}
