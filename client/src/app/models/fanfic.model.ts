import {ChapterModel} from './chapter.model';
import {TagModel} from 'ngx-chips/core/accessor';
import {CommentModel} from './comment.model';

export class FanficModel {

  public id: number;
  public UserId: number;
  public title: string;
  public description: string;
  public image: string;
  public genre: string;
  public rating: number;
  public Chapters: ChapterModel[];
  public Comments: CommentModel[];
  public Tags: TagModel[];

  constructor() {  }

}
