import {L10nConfig, ProviderType, StorageStrategy, TranslationModule} from 'angular-l10n';

export const l10nConfig: L10nConfig = {
  locale: {
    languages: [
      { code: 'en', dir: 'ltr' },
      { code: 'ru', dir: 'ltr' }
    ],
    language: 'en',
    storage: StorageStrategy.Cookie
  },
  translation: {
    providers: [
      { type: ProviderType.Static, prefix: './assets/locale/locale-' }
    ],
    caching: true,
    composedKeySeparator: '.',
    missingValue: 'No key'
  }
};

export const ModuleWithLocalization = TranslationModule.forRoot(l10nConfig);
