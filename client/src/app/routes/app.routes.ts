import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RegisterComponent} from '../components/auth/register/register.component';
import {LoginComponent} from '../components/auth/login/login.component';
import {HomeComponent} from '../components/home/home.component';
import {FacebookComponent} from '../components/auth/social/facebook/facebook.component';
import {TwitterComponent} from '../components/auth/social/twitter/twitter.component';
import {VkontakteComponent} from '../components/auth/social/vkontakte/vkontakte.component';
import {ProfileComponent} from '../components/profile/profile.component';
import {AuthGuard} from '../guards/auth.guard';
import {AdminGuard} from '../guards/admin.guard';
import {FanficCreateComponent} from '../components/fanfics/fanfic-create/fanfic-create.component';
import {FanficUpdateComponent} from '../components/fanfics/fanfic-update/fanfic-update.component';
import {UpdateFanficGuard} from '../guards/update-fanfic.guard';
import {FanficGetComponent} from '../components/fanfics/fanfic-get/fanfic-get.component';
import {DashboardMainComponent} from '../components/dashboard/dashboard-main/dashboard-main.component';
import {UserProfileComponent} from '../components/dashboard/user-profile/user-profile.component';
import {DashboardCreateFanficComponent} from '../components/dashboard/dashboard-create-fanfic/dashboard-create-fanfic.component';
import {DashboardUserCreateComponent} from '../components/dashboard/dashboard-user-create/dashboard-user-create.component';
import {SearchComponent} from '../components/fanfics/search/search.component';
import {GetFanficByTagComponent} from '../components/fanfics/get-fanfic-by-tag/get-fanfic-by-tag.component';
import {GetFanficByCategoryComponent} from '../components/fanfics/get-fanfic-by-category/get-fanfic-by-category.component';


export const router: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'fanfic/create',
    component: FanficCreateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'fanfic/update/:id',
    component: FanficUpdateComponent,
    canActivate: [AuthGuard, UpdateFanficGuard]
  },
  {
    path: 'fanfic/get/:id',
    component: FanficGetComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'dashboard',
    component: DashboardMainComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'dashboard/user/create',
    component: DashboardUserCreateComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'dashboard/user/:id',
    component: UserProfileComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'dashboard/user/:id/fanfic/create',
    component: DashboardCreateFanficComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'facebook',
    component: FacebookComponent
  },
  {
    path: 'twitter',
    component: TwitterComponent
  },
  {
    path: 'vkontakte',
    component: VkontakteComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'fanfics/tag/:tag',
    component: GetFanficByTagComponent
  },
  {
    path: 'fanfics/genre/:genre',
    component: GetFanficByCategoryComponent
  }
];

export const RoutesModule: ModuleWithProviders = RouterModule.forRoot(router);
