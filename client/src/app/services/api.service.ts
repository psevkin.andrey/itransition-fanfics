import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CommentModel} from '../models/comment.model';
import {FanficModel} from '../models/fanfic.model';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable()
export class ApiService {

  private baseUrl = 'http://localhost:3001/api/';

  constructor(private http: HttpClient) {
  }

  registerUser(user: any): Observable<any> {
    return this.http.post(`${this.baseUrl}users/create`, user, httpOptions);
  }

  getLikes(id: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}comments/get/likes?UserId=${id}`);
  }

  unlike(userId: number, commentId: number): Observable<any> {
    return this.http
      .put<any>(`${this.baseUrl}comments/get/${commentId}/unlike`,
        {UserId: userId, CommentId: commentId}, httpOptions);
  }

  like(userId: number, commentId: number): Observable<any> {
    return this.http
      .put<any>(`${this.baseUrl}comments/get/${commentId}/like`,
        {UserId: userId, CommentId: commentId}, httpOptions);
  }

  createComment(comment: any): Observable<CommentModel> {
    return this.http.post<CommentModel>(`${this.baseUrl}comments/create`, comment);
  }

  search(search: string): Observable<FanficModel[]> {
    return this.http.get<FanficModel[]>(`${this.baseUrl}search/${search}`);
  }
}
