import {Injectable} from '@angular/core';
import {UserModel} from '../models/user.model';
import {JwtHelper} from 'angular2-jwt';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AlertService} from './alert.service';
import {Router} from '@angular/router';
import {FanficModel} from '../models/fanfic.model';
import {UserService} from './user.service';
import {FanficService} from './fanfic.service';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class AuthService {

  private baseUrl = 'http://localhost:3001/api/';

  public user: UserModel;
  public fanfics: FanficModel[];
  private token: string;

  private jwtHelper: JwtHelper = new JwtHelper();

  loggedIn: boolean;
  loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);

  constructor(private http: HttpClient,
              private alert: AlertService,
              private router: Router,
              private userService: UserService) {
    const token = localStorage.getItem('token');
    if (token) {
      this.setSession(token);
    }
  }

  public signWithEmail(email: string, password: string): Observable<any> {
    const userInfo = {
      username: email,
      password: password
    };
    return this.http
      .post<any>(`${this.baseUrl}users/login`, userInfo, httpOptions)
      .map((user: any) => {
        this.setSession(user.token);
        this.setLoggedIn(true);
      });
  }

  public signWithSocial(social: string): void {
    window.location.href = `http://localhost:3001/api/auth/${social}`;
    // this.http.get(`${this.baseUrl}auth/${social}`).subscribe();
  }

  private setLoggedIn(value: boolean) {
    this.loggedIn$.next(value);
    this.loggedIn = value;
  }

  private validateToken(token: string): boolean {
    return token.split('.').length === 3;
  }

  private setSession(token: string) {

    let user = null;

    if (this.validateToken(token)) {
      user = this.jwtHelper.decodeToken(token)['user'];
      this.user = new UserModel(user.id,
        user.email,
        user.username,
        user.image,
        user.blocked,
        user.verified_email,
        user.admin);
    }

    if (user && user.verified_email) {
      this.token = token;
      localStorage.setItem('token', token);
      this.userService.getUser(this.user.id).subscribe(usr => {
        this.fanfics = usr.Fanfics;
      }, err => {
        if (err.status === 401) {
          console.log('You are not authorized!');
        }
      });
    } else {
      this.user = null;
      this.alert.error('Email not verified!', true);
    }
  }

  logout() {
    localStorage.removeItem('token');
    this.token = null;
    this.user = undefined;
    this.setLoggedIn(false);
    this.router.navigate(['/']);
  }

  get authenticated(): boolean {

    if (this.token) {
      return !this.jwtHelper.isTokenExpired(this.token);
    }

    return false;
  }

  callbackSocial(social: string, code: string): Observable<any> {
    return this.http
      .get(`${this.baseUrl}auth/${social}/callback?code=${code}`)
      .map((user: any) => {
        this.setSession(user.token);
        this.setLoggedIn(true);
      });
  }

  callbackTwitter(token: string) {
    this.setSession(token);
    this.setLoggedIn(true);
  }
}
