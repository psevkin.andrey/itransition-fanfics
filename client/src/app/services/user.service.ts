import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {UserModel} from '../models/user.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class UserService {

  private baseUrl = 'http://localhost:3001/api/users/';

  constructor(private http: HttpClient) {
  }

  registerUser(user: FormData): Observable<any> {
    return this.http.post(`${this.baseUrl}create`, user);
  }

  getUser(id: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}get/${id}`, httpOptions);
  }

  updateUser(user: FormData, id: number): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}update/${id}`, user);
  }

  getUsers(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(this.baseUrl);
  }

  setAdmin(id: number, status: boolean): Observable<UserModel> {
    return this.http.put<UserModel>(`${this.baseUrl}admin/${id}`, {admin: status});
  }

  blockUser(id: number, status: boolean): Observable<UserModel> {
    return this.http.put<UserModel>(`${this.baseUrl}block/${id}`, {blocked: status});
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}delete/${id}`);
  }
}
