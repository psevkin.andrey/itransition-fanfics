import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {TagModel} from 'ngx-chips/core/accessor';
import {FanficModel} from '../models/fanfic.model';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class FanficService {

  public UserId: number;

  private baseUrl = 'http://localhost:3001/api/fanfics/';

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}delete/${id}`);
  }

  getTags(): Observable<TagModel[]> {
    return this.http.get<TagModel[]>(`${this.baseUrl}tags`);
  }

  createFanfic(fd: FormData): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}create`, fd);
  }

  getFanfic(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}get/${id}`);
  }

  updateFanfic(fd: FormData, id: number) {
    return this.http.put(`${this.baseUrl}update/${id}`, fd);
  }

  getAllRates(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}rating?UserId=${id}`);
  }

  rateChapter(rating: any): Observable<any> {
    return this.http.post(`${this.baseUrl}rate`, rating, httpOptions);
  }

  getPopular(qty: number): Observable<FanficModel[]> {
    return this.http.get<FanficModel[]>(`${this.baseUrl}popular?qty=${qty}`);
  }
  getNewest(qty: number) {
    return this.http.get<FanficModel[]>(`${this.baseUrl}newest?qty=${qty}`);
  }

  getFanficsByTag(tag: string) {
    return this.http.get(`${this.baseUrl}tag/${tag}`);
  }

  getFanficsByCategory(genre: string) {
    return this.http.get(`${this.baseUrl}genre/${genre}`);
  }
}
