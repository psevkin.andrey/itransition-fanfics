import { Injectable } from '@angular/core';

@Injectable()
export class ThemeService {

  public isDark = false;
  private theme: string;

  constructor() {
    this.theme = localStorage.getItem('theme');
    if (!this.theme) {
      localStorage.setItem('theme', 'light');
      this.isDark = false;
    } else {
      this.setTheme(this.theme);
    }
  }

  setTheme(theme: string) {
    if (theme === 'dark') {
      this.theme = 'dark';
      this.isDark = true;
    } else {
      this.theme = 'light';
      this.isDark = false;
    }

    localStorage.setItem('theme', this.theme);
  }

}
