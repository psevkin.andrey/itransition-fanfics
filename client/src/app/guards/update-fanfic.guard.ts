import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable()
export class UpdateFanficGuard implements CanActivate {

  constructor(private auth: AuthService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.auth.user && this.auth.user.admin) {
      return true;
    }
    if (this.auth.fanfics) {
      for (let fanfic of this.auth.fanfics) {
        if (fanfic.id === +route.paramMap.get('id')) {
          return true;
        }
      }
    }

    return false;
  }
}
