import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {AlertService} from '../services/alert.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private auth: AuthService,
              private alert: AlertService) {
  }

  canActivate() {

    if (this.auth.authenticated && this.auth.user) {
      if (this.auth.user.blocked) {
        this.alert.error('You are blocked!', true);
        this.router.navigate(['/']);
        this.auth.logout();
        return false;
      } else if (!this.auth.user.verified_email) {
        this.alert.error('Email not verified!', true);
        this.router.navigate(['/']);
        this.auth.logout();
        return false;
      }

      return true;
    }

    this.alert.error('You are not authenticated', true);
    this.router.navigate(['/login']);
    return false;
  }
}
