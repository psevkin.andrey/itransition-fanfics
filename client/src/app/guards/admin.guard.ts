import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private router: Router,
              private auth: AuthService) {
  }

  canActivate() {
    return this.auth.user && this.auth.authenticated && this.auth.user.admin;
  }
}
